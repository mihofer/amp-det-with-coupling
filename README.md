# Amplitude detuning with coupling 

## Getting starting

To run the scripts, first create a Python 3.5 environment and install the required packages using

```bash
python -m pip install -r python_requirements.txt
```

Then, run

```bash
python ./scripts/run.py --file path/to/twiss.tfs
```

to analyse a twiss file, which will then print the amplitude detuning with and without the effect of local coupling to the command line.
In the `./data` folder, there are some example twiss files, and the corresponding results from PTC_normal,
which were used to check the validity of the formulas.