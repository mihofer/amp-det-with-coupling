import numpy as np 


def add_ampdet_col_to_df(df):
    df =df.assign(XdX = df['K3L']/(16*np.pi)*(df['BETX']**2*df['COSH']**4 + 
                                      df['BETY']**2*np.abs(df['FXY'])**4 -
                                      2*df['BETX']*df['BETY']*df['COSH']**2*
                                      (np.abs(df['FXY'])**2 + 2*np.imag(df['FXY'])**2)
                                      ))
    
    df =df.assign(XdX_nocoupl = df['K3L']/(16*np.pi)*(df['BETX']**2))

    df =df.assign(YdY = df['K3L']/(16*np.pi)*(df['BETY']**2*df['COSH']**4 + 
                                      df['BETX']**2*np.abs(df['FYX'])**4 -
                                      2*df['BETX']*df['BETY']*df['COSH']**2*
                                      (np.abs(df['FYX'])**2 + 2*np.imag(df['FYX'])**2)
                                      ))
    df =df.assign(YdY_nocoupl = df['K3L']/(16*np.pi)*(df['BETY']**2))
  
    df =df.assign(XdY = df['K3L']/(8*np.pi)*(df['BETX']**2*df['COSH']**2*np.abs(df['FYX'])**2 + 
                                     df['BETY']**2*df['COSH']**2*np.abs(df['FXY'])**2 -
                                     df['BETX']*df['BETY']*
                                     (df['COSH']**4-
                                      4*df['COSH']**2*np.imag(df['FYX'])*np.imag(df['FXY'])+
                                      np.abs(df['FYX'])**2*np.abs(df['FXY'])**2)
                                     ))
    df =df.assign(XdY_nocoupl = df['K3L']/(8*np.pi)*(-df['BETX']*df['BETY']))

    return df


def get_ampdet(df):
    df = add_ampdet_col_to_df(df)
    return df

    
def print_ampdet_comp(df_twiss):
    print('Amplitude detuning $dQ_x/dJ_x$: Formula:{coupl}, \nAmplitude detuning without coupling $dQ_x/dJ_x$: Formula:{nocoupl}'.format(coupl=df_twiss["XdX"].sum(), nocoupl=df_twiss["XdX_nocoupl"].sum()))
    print('Amplitude detuning $dQ_y/dJ_y$: Formula:{coupl}, \nAmplitude detuning without coupling $dQ_y/dJ_y$: Formula:{nocoupl}'.format(coupl=df_twiss["YdY"].sum(), nocoupl=df_twiss["YdY_nocoupl"].sum()))
    print('Amplitude detuning $dQ_x/dJ_y$: Formula:{coupl}, \nAmplitude detuning without coupling $dQ_x/dJ_y$: Formula:{nocoupl}'.format(coupl=df_twiss["XdY"].sum(), nocoupl=df_twiss["XdY_nocoupl"].sum()))
    print('')
