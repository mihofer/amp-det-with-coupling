from twiss_optics import optics_class
import numpy as np 
import pandas as pd


def add_coupling_rdt_to_df(df):
    optics = optics_class.TwissOptics(df)
    rdts = optics.get_coupling(method='cmatrix')
    # rdts.loc[:, "F1001"].real *= -1
    # rdts.loc[:, "F1010"].real *= -1
    df = pd.merge(df, rdts, on='S', left_index=True, how='inner')
    return df


def add_sum2_diff2_to_df(df):
    df = df.assign(sum2_diff2 = (np.abs(2*df['F1010'])**2 - np.abs(2*df['F1001'])**2))
    return df


def add_cosh_to_df(df):
    df =df.assign(COSH= np.where(df['sum2_diff2'] > 0,
                          np.cosh(np.sqrt(df['sum2_diff2'])),
                          np.cos(np.sqrt(np.abs(df['sum2_diff2']))))) 
    return df


def add_P_to_df(df):
    df =df.assign(P = np.where(np.abs(df['sum2_diff2'])>1e-12,
                       0.5*np.sqrt(np.abs(df['sum2_diff2'])),
                       1) )
    return df


def add_sinh_to_df(df):
    df =df.assign(SINH = np.where(df['sum2_diff2']>0,
                          np.sinh(np.sqrt(np.abs(df['sum2_diff2'])))/df['P'],
                          np.sin(np.sqrt(np.abs(df['sum2_diff2'])))/df['P']))
    return df


def add_fxy_fyx_to_df(df):
    df =df.assign(FYX = df['SINH']*(np.conj(df['F1001'])-np.conj(df['F1010'])))
    df =df.assign(FXY = df['SINH']*(df['F1001']-np.conj(df['F1010'])))
    return df


def add_coupling_terms_to_df(df):
    df = df.drop(labels=['F1001', 'F1010', 'COSH', 'SINH', 'FXY', 'FYX', 'P', 'sum2_diff2'], axis=1, errors='ignore')
    df = add_coupling_rdt_to_df(df)
    df = add_sum2_diff2_to_df(df)
    df = add_cosh_to_df(df)
    df = add_P_to_df(df)
    df = add_sinh_to_df(df)
    df = add_fxy_fyx_to_df(df)
    return df
