import numpy as np


def add_beamsize_to_df(df, emitx,emity):
    df = df.assign(
        beamsize_x = np.real(
        np.sqrt( df['BETX']*emitx*df['COSH']**2 + df['BETX']*emity*np.abs(df['FYX'])**2 )
        ))
    df = df.assign(
        beamsize_y = np.real(
        np.sqrt( df['BETY']*emity*df['COSH']**2 + df['BETY']*emitx*np.abs(df['FXY'])**2 )
        ))
    return df


def add_tilt_to_df(df, emitx, emity):
    df = df.assign(
        tilt = 0.5*np.arctan(
        2*(np.sqrt(df['BETX']*df['BETY'])*df['COSH']*(
        emitx*np.imag(df['FXY']) + emity*np.imag(df['FYX'])
        )
        )/(
        ( df['BETX']*emitx*df['COSH']**2+df['BETY']*emity*np.abs(df['FYX'])**2 ) - 
        ( df['BETY']*emity*df['COSH']**2+df['BETX']*emitx*np.abs(df['FXY'])**2 )
        )
        ))
    return df

def get_beamsize_and_tilt(df, emit_x, emit_y):

    df = add_beamsize_to_df(df, emit_x, emit_y)
    df = add_tilt_to_df(df, emit_x, emit_y)
    return df