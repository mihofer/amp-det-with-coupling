import pytest
import numpy as np
from utils import coupling_terms
from utils import amp_det_coupling
from utils import beam_size_coupling
from tfs_files import tfs_pandas


@pytest.mark.parametrize('arc', ['a12_skew', 'a12', 'a23_skew', 'a23'])
def test_amp_det_coupling(arc):
    print('./data/twiss_{}.tfs'.format(arc))
    twiss_df = tfs_pandas.read_tfs('../data/twiss_{}.tfs'.format(arc), index='NAME')
    twiss_df = coupling_terms.add_coupling_terms_to_df(twiss_df)
    twiss_df = amp_det_coupling.get_ampdet(twiss_df)
    df_ptc = tfs_pandas.read_tfs('../data/ptc_{}.dat'.format(arc))

    xdx = df_ptc.loc[ (df_ptc["NAME"] == "ANHX") & (df_ptc["ORDER1"] == 1) & (df_ptc["ORDER2"] == 0)]["VALUE"].to_numpy()*2
    ydy = df_ptc.loc[ (df_ptc["NAME"] == "ANHY") & (df_ptc["ORDER1"] == 0) & (df_ptc["ORDER2"] == 1)]["VALUE"].to_numpy()*2
    xdy = df_ptc.loc[ (df_ptc["NAME"] == "ANHX") & (df_ptc["ORDER1"] == 0) & (df_ptc["ORDER2"] == 1)]["VALUE"].to_numpy()*2

    assert (twiss_df["XdX"].sum() - xdx)/xdx < 2e-2
    assert (twiss_df["YdY"].sum() - ydy)/ydy < 2e-2
    assert (twiss_df["XdY"].sum() - xdy)/xdy < 2e-2


@pytest.mark.parametrize('twiss', ['ip_no_coupling', 'ip_coupling'])
def test_beamsize_coupling(twiss):
    print('./data/twiss_{}.tfs'.format(twiss))
    twiss_df = tfs_pandas.read_tfs('../data/twiss_{}.tfs'.format(twiss), index='NAME')
    twiss_df = coupling_terms.add_coupling_terms_to_df(twiss_df)
    emittance_x=1
    emittance_y=2
    twiss_df = beam_size_coupling.get_beamsize_and_tilt(twiss_df, emittance_x, emittance_y)

    bs_tracking={
        'ip_no_coupling':{
        'x': np.sqrt(twiss_df.loc["IP1", "BETX"]).iloc[0]*emittance_x, 
        'y': np.sqrt(twiss_df.loc["IP1", "BETY"]).iloc[0]*emittance_y
        },
        'ip_coupling':{
        'x': 2.200258*np.sqrt(twiss_df.loc["IP1", "BETX"]).iloc[0]*emittance_x, 
        'y': 1.399027*np.sqrt(twiss_df.loc["IP1", "BETY"]).iloc[0]*emittance_y
        },
    }

    assert (((twiss_df.loc["IP1", "beamsize_x"])).iloc[0] - bs_tracking[twiss]['x'])/bs_tracking[twiss]['x'] < 2e-2
    assert (((twiss_df.loc["IP1", "beamsize_y"])).iloc[0] - bs_tracking[twiss]['y'])/bs_tracking[twiss]['y'] < 2e-2


@pytest.mark.parametrize('twiss', ['ip_no_coupling', 'ip_coupling'])
def test_tilt_coupling(twiss):
    print('./data/twiss_{}.tfs'.format(twiss))
    twiss_df = tfs_pandas.read_tfs('../data/twiss_{}.tfs'.format(twiss), index='NAME')
    twiss_df = coupling_terms.add_coupling_terms_to_df(twiss_df)
    emittance_x=1
    emittance_y=2
    twiss_df = beam_size_coupling.get_beamsize_and_tilt(twiss_df, emittance_x, emittance_y)

    tilt={
        'ip_no_coupling': 0 ,
        'ip_coupling': -0.000435,
    }

    assert (((twiss_df.loc["IP1", "tilt"]) - tilt[twiss]).iloc[0]) < 2e-6
    
    
    
    