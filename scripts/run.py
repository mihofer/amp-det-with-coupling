
import argparse
import os
import sys

sys.path.append(os.path.abspath(__file__))
from utils import coupling_terms
from utils import amp_det_coupling
from utils import beam_size_coupling
from tfs_files import tfs_pandas

def main(args):
    print('Loading twiss file {}.'.format(args.file))
    twiss_df = tfs_pandas.read_tfs(args.file, index='NAME')
    twiss_df = coupling_terms.add_coupling_terms_to_df(twiss_df)
    twiss_df = amp_det_coupling.get_ampdet(twiss_df)
    amp_det_coupling.print_ampdet_comp(twiss_df)
    twiss_df = beam_size_coupling.get_beamsize_and_tilt(twiss_df, args.emit[0], args.emit[1])

    return twiss_df, twiss_df["XdX"].sum(), twiss_df["YdY"].sum(), twiss_df["XdY"].sum()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Give file for computation of observables. in presence of coupling.')
    parser.add_argument('--file', type=str, help='filepath')
    parser.add_argument('--emit', type=float, nargs=2, help='emittance', default=[0,0])

    args = parser.parse_args()
    main(args)
